# coding: utf-8

require 'json'
require 'gdbm'

class Json_result

	def self.rooms(name = nil)
		data = []
		rooms = []; Dir.glob('pv/**/*.gdbm') {|entry|
			entry =~ /\/(_[^\/]+)\.gdbm$/ or next
			room = $1
			room =~ /^_[tc]~(.+)/ or next
			rooms << CGI.unescape($1)
		}
		entry = {'rooms' => rooms}
		data << entry
		JSON.dump(data)
	end
end

__END__

