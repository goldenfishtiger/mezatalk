require 'uri'

class WebSocketHelper

	attr_reader :uri

	def initialize(uri)
		@uri = URI.parse(uri)
	end

	def handshake
		(<<REQ % [@uri.path, @uri.host, @uri.port, make_websocket_key]).gsub(/\n/, "\r\n")
GET %s HTTP/1.1
Host: %s:%s
Upgrade: websocket
Connection: upgrade
Sec-WebSocket-Key: %s
Sec-WebSocket-Version: 13

REQ
	end

	def make_websocket_key
		nonce = []; 4.times { nonce << rand(0xFFFFFFFF) }
		@websocket_key = [nonce.pack('N*')].pack('m0')
	end

	def encode(req)
		make_masking_key
		head(req) + payload(req)
	end

	def make_masking_key
		@masking_key = rand(0xFFFFFFFF)
	end

	def head(req)
		head = ''

		fopc = 0
		fopc += (fin = 1) << 7
		fopc += (opcode = 1)
		head << [fopc].pack('C')

		mplen = 0
		mplen += (mask = 1) << 7
		if((it = req.length) < 126)
			mplen += it
			head << [mplen].pack('C')
		elsif(it < 65536)
			mplen += 126
			head << [mplen, it].pack('Cn')
		else
			mplen += 127
			head << [mplen, 0, it].pack('CNN')
		end

		if(mask == 1)
			head << [@masking_key].pack('N')
		end

		head
	end

	def payload(req0)
		len0 = req0.length; req = req0.dup
		req << "\x00" while(req.length % 4 != 0)
		res = []; req.unpack('N*').each {|u32|
			res << (u32 ^ @masking_key)
		}
		res.pack('N*')[0, len0]
	end

	def decode(res)
		fopc = res.slice!(0, 1)
		mplen = res.slice!(0, 1).unpack('C')[0]
		if(mplen < 126 and mplen == res.length)
		elsif(mplen == 126 and (res.slice!(0, 2).unpack('n')[0]) == res.length)
		elsif(mplen == 127 and (res.slice!(0, 8).unpack('NN')[1]) == res.length)
		else
			raise('Unexpected.')
		end
		res
	end
end

__END__

