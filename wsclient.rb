# coding: utf-8

require 'em-websocket-client'

class ClientWebSocket

	def initialize(uri)
		@uri = uri
		@seq = -1
		@messages = []
	end

	def open
		EM.run {
			socket = EventMachine::WebSocketClient.connect(@uri)

			socket.callback {
				yield([@seq += 1, nil])
				socket.send_msg(@messages.shift)
			}

			socket.errback {|e|
				raise('Connect error. [%s]'% e.inspect)
			}

			socket.stream {|message|
				yield([@seq += 1, message.data.force_encoding('UTF-8')])
				(request = @messages.shift) ? socket.send_msg(request) : socket.close_connection
			}

			socket.disconnect {
				EM::stop_event_loop
			}
		}
	end

	def push_msg(message)
		@messages << message
	end
end

__END__

