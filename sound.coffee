#-------------------------------------------------------------------------------
#
#	サウンド関係定義
#
class Sounds

	constructor: ->
		@enable = document.createElement('audio').canPlayType
		@sounds = {}

	set_src: (name, src) ->
		@sounds[name] = new Sound(src, @enable)

	ready: ->
		for name, sound of @sounds
			if(sound.getReadyState() != 4) then return(false)	# 4: HAVE_ENOUGH_DATA
		true

	bgm: (name) -> @sounds[name]

	se:  (name) -> @sounds[name]

	off: ->
		for name, sound of @sounds
			sound.off()

	d0: ->
		for name, sound of @sounds
			sound.d0()											# TODO: sound.tasks.size > 0 and sound.do

#-------------------------------------------------------------------------------
#
#	各サウンド
#
class Sound

	constructor: (src, @enable) ->
		@audio = new Audio(src) if(@enable)

	getReadyState: ->
		unless(@enable) then return(4)							# 4: HAVE_ENOUGH_DATA
		@audio.readyState

	play: (@times = 1) ->
		unless(@enable) then return
		@off(); @audio.play()
		--@times

	out: -> @play()

	loop_play: -> @play(99999)

	pause: ->
		@audio.pause() if(@enable)

	off: ->
		unless(@enable) then return
		@audio.pause()
		@audio.currentTime = 0

	setVolume: (@vol) ->
		unless(@enable) then return
		@audio.volume = @vol / 100
		if(@vol == 0) then @off()

	setCurrentTime: (t) ->
		@audio.currentTime = t if(@enable)

	d0: ->
		unless(@enable) then return
		if(@audio.ended)
			if(--@times > -1) then @audio.play() else @off()

