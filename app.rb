# http://sinatrarb.com/intro-ja.html

require 'sinatra'

eval(File.read('pv/mezatalk.config')) rescue true
@configs ||= {}

require './auth' unless(@configs[:no_auth])

get('/gaze') {
	response['Cache-Control'] = 'no-cache'
	erb :gaze
}

get('/talk') {
	response['Cache-Control'] = 'no-cache'
	erb :talk
}

get('/setting') {
	response['Cache-Control'] = 'no-cache'
	erb :setting
}

post('/upload') {
	erb :upload
}

get('/search') {
	response['Cache-Control'] = 'no-cache'
	erb :search
}

post('/api/v1/rooms/:name') {
	erb :rest
}

post('/api/v1/rooms/:name/:seq') {
	erb :rest_get
}

get('/qapi/v1/rooms') {
	erb :qapi
}

get('/qapi/v1/rooms/:name') {
	erb :qapi
}

get('/*') {
	'?'
}

__END__

