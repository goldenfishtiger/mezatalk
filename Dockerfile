FROM docker.io/goldenfishtiger/sinatra_skelton

LABEL maintainer="Cnes Taro <goldenfishtiger@gmail.com>"

ARG http_proxy
ARG https_proxy

USER root
WORKDIR /

# https://medium.com/nttlabs/ubuntu-21-10-and-fedora-35-do-not-work-on-docker-20-10-9-1cd439d9921
#SHELL ["/clone3-workaround", "/bin/bash", "-c"]

RUN set -x \
	&& dnf install -y \
		gdbm-devel \
		npm \
		ImageMagick \
		ImageMagick-devel \
		git \
		rsync \
		openssh-clients \
	&& rm -rf /var/cache/dnf/* \
	&& dnf clean all

# git clone mezatalk しておくこと
ARG TARGET=mezatalk
RUN set -x \
	&& ln -s /home/user/sinatra_skelton /home/user/${TARGET}
COPY ${TARGET}/Gemfile* /home/user/${TARGET}/
RUN set -x \
	&& chown -R user:user /home/user
USER user
WORKDIR /home/user/${TARGET}
RUN set -x \
	&& bundle config set path ../bundle \
	&& bundle install \
	&& bundle clean -V

RUN set -x \
	&& git clone https://itline.jp/git/ngs

USER root
RUN set -x \
	&& npm install --global coffeescript
COPY ${TARGET} /home/user/${TARGET}
RUN set -x \
	&& coffee -b -c *.coffee \
	&& curl http://code.jquery.com/jquery-2.1.3.min.js > public/jquery.js \
	&& chown -R user:user /home/user/${TARGET}/

EXPOSE 8080
EXPOSE 33109

# Dockerfile 中の設定スクリプトを抽出するスクリプトを出力、実行
COPY Dockerfile .
RUN echo $'\
cat Dockerfile | sed -n \'/^##__BEGIN0/,/^##__END0/p\' | sed \'s/^#//\' > startup.sh\n\
' > extract.sh && bash extract.sh

# docker-compose up の最後に実行される設定スクリプト
##__BEGIN0__startup.sh__
#
#	CONFIG=mezatalk.config
#	if [ ! -e pv/$CONFIG ]; then
#		echo "# $CONFIG.sample set up."
#		cp -av $CONFIG.sample pv
#		echo "Rename 'pv/$CONFIG.sample' to 'pv/$CONFIG' and modify it."
#		echo '**** HALT ****'
#		sleep infinity
#	fi
#
#	if [ -e pv/dot.bashrc ]; then
#		cp -av pv/dot.bashrc /home/user/.bashrc
#		cp -av pv/dot.virc /home/user/.virc
#		cp -av pv/dot.gitconfig /home/user/.gitconfig
#	fi
#
#	mkdir -pv pv/uploads
#
#	if [ -v BACKUP_HOST ]; then
#		mkdir -v ~/.ssh
#		cp -v pv/id_rsa ~/.ssh
#	fi
#
#	ln -sv ngs/ngs.rb .
#
#	# easy cron
#	now=`date +%s`
#	target0=$((now - now % ${CYCLE0:=86400}   + ${SCHED0:=$(( 5 * 60 +  3 * 3600 - 32400))}))
#	while [ $target0 -lt $now ]; do
#		((target0 += CYCLE0))
#	done
#	target1=$((now - now % ${CYCLE1:=86400}   + ${SCHED1:=$(( 5 * 60 +  4 * 3600 - 32400))}))
#	while [ $target1 -lt $now ]; do
#		((target1 += CYCLE1))
#	done
#
#	cd themes
#	for dir in `ls`; do
#		for scale in ${TSCALES}; do
#			../bslight $dir $scale
#		done
#	done
#	cd ..
#
#	s=S
#	while true; do
#		pgrep -f mezatalk_wss > /dev/null
#		if [ $? -ne 0 ]; then
#			echo "`date`: ${s}tart mezatalk_wss."
#			bundle exec mezatalk_wss &
#		fi
#		pgrep -f puma > /dev/null
#		if [ $? -ne 0 ]; then
#			echo "`date`: ${s}tart puma."
#			bundle exec rackup -P /tmp/rack.pid --host 0.0.0.0 --port 8080 &
#		fi
#		pgrep -f wsclient_remind > /dev/null
#		if [ $? -ne 0 ]; then
#			echo "`date`: ${s}tart wsclient_remind."
#			bundle exec wsclient_remind &
#		fi
#		s=Res
#
#		# easy cron
#		if [ `date +%s` -ge $target0 ]; then
#			((target0 += CYCLE0))
#			echo "`date`: Job easy cron 0 started."
#			bundle exec export_rooms
#		fi
#		if [ -v BACKUP_PATH ]; then
#			if [ `date +%s` -ge $target1 ]; then
#				((target1 += CYCLE1))
#				echo "`date`: Job easy cron 1 started."
#				stamp=`date +%Y%m%d_%H%M`
#				ssh_opt='-o StrictHostKeyChecking=no -o PasswordAuthentication=no'
#				if [ -v BACKUP_HOST ]; then
#					coron=:
#					ssh_opr="ssh $ssh_opt $BACKUP_HOST"
#				fi
#				last_backup=`$ssh_opr ls $BACKUP_PATH | tail -1`
#				backup_ex0='--exclude=hyperestraier'
#				if [ -z "$last_backup" ]; then
#					gen=0
#				else
#					gen=$((`$ssh_opr cat $BACKUP_PATH/$last_backup/.gen` + 1))
#					link_dest="--link-dest=../$last_backup"
#				fi
#				rsync -av --delete $backup_ex0 $BACKUP_EX -e "ssh $ssh_opt" pv $BACKUP_HOST$coron$BACKUP_PATH/$stamp $link_dest
#				echo $gen > .gen; scp $ssh_opt .gen $BACKUP_HOST$coron$BACKUP_PATH/$stamp
#			fi
#		fi
#		sleep 5
#	done
#
##	target0=$((now - now % ${CYCLE0:=3600}    + ${SCHED0:=$(( 0 * 60))}))									# hour
##	target0=$((now - now % ${CYCLE0:=86400}   + ${SCHED0:=$(( 0 * 60 +  0 * 3600 - 32400))}))				# day
##	target0=$((now - now % ${CYCLE0:=604800}  + ${SCHED0:=$(( 0 * 60 +  0 * 3600 + 0 * 86400 - 378000))}))	# week
##	target0=$((now - now % ${CYCLE0:=2419200} + ${SCHED0:=$(( 0 * 60 +  0 * 3600 + 0 * 86400 - 378000))}))	# 4weeks
#
##__END0__startup.sh__

USER user

ENTRYPOINT ["bash", "-c"]
CMD ["bash startup.sh"]

