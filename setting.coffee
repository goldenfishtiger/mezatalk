$ ->

	# 試しに音を鳴らす
	$('#s_test').on('click', ->
		sounds = new Sounds
		sound = sounds.set_src('TEST', 'sounds/' + $('select[name="sound"]').val())
		sound.setVolume($('select[name="volume"]').val())
		sounds.se('TEST').out()
	)

	# 音色変更
	$('#s_sound').change ->
		if($('#s_sound').val() == 'OFF')
			$('#s_volume').prop('disabled', true)
			$('#s_volume').val(0)
			$('#s_test').prop('disabled', true)
		else
			$('#s_volume').prop('disabled', false)
			if($('#s_volume').val() == '0')
				$('#s_volume').val(5)
			$('#s_test').prop('disabled', false)

	# 音量変更
	$('#s_volume').change ->
		if($('#s_volume').val() == '0')
			$('#s_sound').val('OFF')
			$('#s_volume').prop('disabled', true)
			$('#s_test').prop('disabled', true)

	0

