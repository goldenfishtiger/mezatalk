# coding: utf-8

=begin

* email.rb

	メールを送信する

=end

#require 'base64'
require 'kconv'
#require 'net/pop'
require 'net/smtp'

#===============================================================================
#
#	String クラス
#
class String

	#-----------------------------------------------------------
	#
	#	件名等の BASE64 をデコードする
	#
	def b_decode
		self.gsub(/=\?ISO-2022-JP\?B\?([!->@-~]+)\?=/i) {
			$1.unpack("m")[0]
		}.gsub(/\t|\n/, '').gsub(/\0/, '')
	end

	#-----------------------------------------------------------
	#
	#	件名等を BASE64 エンコードする
	#
	#		自動的に 60 文字で切る必要がある
	#
	#		ある程度以上の長さの英数字の連続を検知したい
	#
	def b_encode
		'=?ISO-2022-JP?B?' + [self.tojis].pack('m*').gsub(/\n/, '') + '?='
	end

	#-----------------------------------------------------------
	#
	#	文末の引用部を自動削除する
	#
	def strip_quote(form = '＜引用部 %d 行、自動削除＞')
		body = self.split(/\r?\n/).reverse

		from = to = gap = nil
		body.each_index {|n|
			from = n if(!from and body[n] =~ /^\s*(>|＞)/)			# 最初の > を見つける
			to = n and gap = 0 if(from and body[n] =~ /^\s*(>|＞)/)	# 連続する > を見つける
			gap += 1 if(from and body[n] !~ /^\s*(>|＞)/)			# > の抜けを検出する
			break if(from and gap > 2)								# n 行以上の > の抜けを検出したら確定
		}

		body[from..to] = sprintf(form, to - from + 1) if(from and (to - from) > 20)
		body.reverse.join("\n")
	end
end

#===============================================================================
#
#	Email クラス
#
class Email

	attr_reader :body

	#-----------------------------------------------------------
	#
	#	initialize メールオブジェクトを生成する
	#
	def initialize(configs, mail = nil)		# mail = Array or IO(ex.STDIN) or nil

		@configs = configs
		@header  = {}
		@body    = []

		if(mail)
			key    = nil
			inBody = false
			mail.each {|line|
				line.chomp!
				unless inBody
					if line =~ /^$/					# 空行
						inBody = true
					elsif line =~ /^(\S+?):\s*(.*)/	# ヘッダ行
						key = $1.capitalize
						@header[key] = $2
					elsif key						# ヘッダ行が2行に渡る場合
						@header[key] += "\n" + line.sub(/^\s*/, "\t")
					end
				else
					@body.push(line)				# メールボディ
				end
			}
		end
	end

	#-----------------------------------------------------------
	#
	#	[] ヘッダを参照
	#
	def [](key)
		@header[key.capitalize]
	end

	#-----------------------------------------------------------
	#
	#	[]= ヘッダを設定
	#
	def []=(key, value)
		@header[key.capitalize] = value
	end

	#-----------------------------------------------------------
	#
	#	<< ボディにテキストを追加
	#
	def <<(message)
		@body.push message
	end

	#-----------------------------------------------------------
	#
	#	header_each メールヘッダを順に渡す
	#
	def header_each
		@header.each {|key, value|
			yield("#{key}: #{value}\n")
		}
	end

	#-----------------------------------------------------------
	#
	#	body_each メールボディを順に渡す
	#
	def body_each
		if(@header['Content-transfer-encoding'] =~ /quoted-printable/i)
			ll = ''; @body.each {|l|
				ll << l + "\n"
			}
			ll.unpack('M')[0].split(/\r?\n/).each {|l|
				yield(l.force_encoding('UTF-8') + "\n")
			}
		else
			@body.each {|l|
				yield(l.force_encoding('UTF-8') + "\n")
			}
		end
	end

	#-----------------------------------------------------------
	#
	#	each メールデータを順に渡す
	#
	def each
		self.header_each {|h|
			yield(h)
		}
		yield("\n")								# ヘッダ/ボディのセパレータ
		self.body_each {|l|
			yield(l.force_encoding('UTF-8'))
		}
	end

	#-----------------------------------------------------------
	#
	#	decode_date デートをデコードする
	#
	@@month = Hash[	'jan',  1, 'feb',  2, 'mar',  3, 'apr',  4, 'may',  5, 'jun',  6,
					'jul',  7, 'aug',  8, 'sep',  9, 'oct', 10, 'nov', 11, 'dec', 12]
	def decode_date
		date = nil
		if(@header['Date'] =~ /(\d+)\s+(\w{3})\s+(\d{4})\s+(\d+):(\d+):(\d+)/)
			date = Hash.new
			date['day']  = $1.to_i; date['mon']  = @@month[$2.downcase]
			date['year'] = $3.to_i; date['hour'] = $4.to_i
			date['min']  = $5.to_i; date['sec']  = $6.to_i
		end
		date
	end

	def clear_body
		@body = []
	end

	#-----------------------------------------------------------
	#
	#	send メールを送る
	#
	def send(from = nil, *to)
		from  = @header['From'] unless from
		to.push @header['To']   if to.size == 0
		smtp = Net::SMTP.new(@configs[:SMTP_SERVER], @configs[:SMTP_PORT])
		if(@configs[:SMTP_OVER_TLS] || @configs[:SMTP_START_TLS])
			ssl = OpenSSL::SSL::SSLContext.new
			(it = @configs[:SMTP_TLS_VERIFY]) and ssl.verify_mode = it
			(it = @configs[:SMTP_TLS_CAFILE]) and ssl.ca_file     = it
			@configs[:SMTP_OVER_TLS]  and smtp.enable_tls(ssl)
			@configs[:SMTP_START_TLS] and smtp.enable_starttls(ssl)
		end
		smtp.start( @configs[:SMTP_HELO],
					@configs[:SMTP_ACCOUNT],
					@configs[:SMTP_PASSWORD],
					@configs[:SMTP_AUTHTYPE]) {|smtp|
			smtp.send_mail(self, from, *to)
		}
	end
end

#===============================================================================
#
#	EncodedEmail クラス
#
class EncodedEmail < Email

	#-----------------------------------------------------------
	#
	#	<< ボディにコンテントを追加(IOインスタンスを渡す)
	#
	def <<(io)
		@body = io
	end

	#-----------------------------------------------------------
	#
	#	body_each メールボディを順に渡す(ストリームから受け取る場合、受けられるのは1度)
	#
	def body_each
		if self['Content-Transfer-Encoding'] =~ /base64/i
			@body.each {|l|
				yield(decode64(l))
			}
		else
			super
		end
	end

	#-----------------------------------------------------------
	#
	#	each メールデータを順に渡す
	#
	def each
		if self['Content-Transfer-Encoding'] =~ /base64/i
			self.header_each {|l|
				yield(l)
			}
			yield("\n")							# ヘッダ/ボディのセパレータ
			while(l = @body.read(45))
				yield(encode64(l))
			end
		else
			super
		end
	end
end

#===============================================================================
#
#	AttachedEmail クラス
#
class AttachedEmail < EncodedEmail

	attr_reader	:block

	#-----------------------------------------------------------
	#
	#	Initialize 添付メールオブジェクトを生成する
	#
	def initialize(configs, mail = nil)

		super(configs, mail)

		if mail == nil
			@separator = "separator" + (rand 65536).to_s
			self['MIME-Version'] = "1.0"
			self['Content-Type'] = "Multipart/Mixed; boundary=\"#{@separator}\""
			@block = []
			return
		end

		return unless self['Content-Type'] =~ /^Multipart\/Mixed;\s*boundary=(["']?)(.*)\1/i
		@separator = $2
		@block = nil
	end

	#-----------------------------------------------------------
	#
	#	block_each メールブロックを順に渡す
	#
	def block_each
		if(@block)
			@block.each {|b|
				yield(b)
			}
		else
			def @body.each						# eachをセパレータで止まる特異メソッドに変更する
				super {|l|
					break if l =~ /^--#{@separator}/
					yield(l)
				}
			end
			loop {
				break if(@body.eof)
				yield(EncodedEmail.new(@body))
			}
		end
	end

	#-----------------------------------------------------------
	#
	#	<< メールブロックを追加
	#
	def <<(block)
		@block.push(block)
	end

	#-----------------------------------------------------------
	#
	#	each メールデータを順に渡す
	#
	def each
		self.header_each {|h|
			yield(h)
		}
		yield("\n")								# ヘッダ/ボディのセパレータ
		@block.each {|b|
			yield("--" + @separator + "\n")
			b.each {|l|
				yield l
			}
		}
		yield("--" + @separator + "--\n")
	end
end

__END__

