# coding: utf-8

=begin

* members.rb

	メンバ一覧

=end

require 'net/ldap'

class Members

	attr_reader :status

	def initialize(configs)
		@configs = configs
		@members = []
		@name2uid = {}
		@status = false
	end

	def load
		begin
			ldap = Net::LDAP.new(
				host:	@configs[:ldap_host],
				port:	@configs[:ldap_port],
			)
			results = ldap.search(
				base:		@configs[:ldap_search_base],
				filter:		@configs[:ldap_search_filter],
			)
			results.sort {|key_a, key_b|
				-(key_a['listOrder'][0].to_i <=> key_b['listOrder'][0].to_i)
			}.each {|result|
				@members << {
					:UID	=> (u = result['uid'][0]),				# ユーザ名(数値ではない)
					:LORDER	=>		result['listOrder'][0].to_i,
					:MAVEID	=> (n = result['maveid'][0].force_encoding('UTF-8')),
					:MAIL	=>		result['mail'][0],
				}
				@name2uid[n] = u
			}
			@status = true
		rescue
		end if(@configs[:ldap_host])

		eval(File.read('pv/members.config')) rescue(true)

		lorder = 900; @names = [
			[	'金剛',		'x-00001@example.com',		],
			[	'比叡',		'x-00002@example.com',		],
			[	'榛名',		'x-00003@example.com',		],
			[	'霧島',		'x-00004@example.com',		],
			[	'扶桑',		'x-00005@example.com',		],
			[	'山城',		'x-00006@example.com',		],
			[	'伊勢',		'x-00007@example.com',		],
			[	'日向',		'x-00008@example.com',		],
			[	'長門',		'x-00009@example.com',		],
			[	'陸奥',		'x-00010@example.com',		],
			[	'大和',		'x-00011@example.com',		],
			[	'武蔵',		'x-00012@example.com',		],
		].each {|name, mail|
			@members << {
				:UID	=> (u = name),
				:LORDER	=>		lorder += 1,
				:MAVEID	=> (n = name),
				:MAIL	=>		mail,
			}
			@name2uid[n] = u
		} if(size < 1)
	end

	def size
		@members.size
	end

	def each(min = 0)
		@members.each {|member|
			yield(member) if(member[:LORDER] > min)
		}
	end

	def each_uid(min = 0)
		each(min) {|member|
			yield(member[:UID])
		}
	end

	def each_name(min = 0)
		each(min) {|member|
			yield(member[:MAVEID])
		}
	end

	def names(min = 0)
		names = []; each(min) {|member|
			names << member[:MAVEID]
		}
		names
	end

	def name2uid(name)
		@name2uid[name]
	end
end

__END__

